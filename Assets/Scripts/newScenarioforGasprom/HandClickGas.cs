﻿using System.Collections;
using UnityEngine;
using Valve.VR;
public class HandClickGas : MonoBehaviour
{
    float startXPosition;
    float valueOfInputX;
    float deltaValueFirst;
    float deltaValueSecond;
    float deltaValueThird;
    bool firstOK;
    bool secondOK;
    bool thirdOK;
    bool isReadyToStart;
    public bool isOK;
    public bool teleportBack;
    
    public bool triggerCommand;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TRIGGERCOMMAND")
        {
            triggerCommand = true;
        }
        if(other.tag == "teleportBack")
        {
            teleportBack = true;
        }
        if (other.tag == "firstRotate")
        {
            if (SteamVR_Actions.default_InteractUI.GetStateDown(SteamVR_Input_Sources.Any))
            {
                startXPosition = this.transform.position.x;
            }
            if (SteamVR_Actions.default_InteractUI.GetStateUp(SteamVR_Input_Sources.Any))
            {
                valueOfInputX = this.transform.position.x;
                deltaValueFirst += valueOfInputX - startXPosition;
            }
        }
        if (other.tag == "secondRotate")
        {
            if (SteamVR_Actions.default_InteractUI.GetStateDown(SteamVR_Input_Sources.Any))
            {
                startXPosition = this.transform.position.x;
            }
            if (SteamVR_Actions.default_InteractUI.GetStateUp(SteamVR_Input_Sources.Any))
            {
                valueOfInputX = this.transform.position.x;
                deltaValueSecond += valueOfInputX - startXPosition;
            }
        }
        if (other.tag == "thirdRotate")
        {
            if (SteamVR_Actions.default_InteractUI.GetStateDown(SteamVR_Input_Sources.Any))
            {
                startXPosition = this.transform.position.x;
            }
            if (SteamVR_Actions.default_InteractUI.GetStateUp(SteamVR_Input_Sources.Any))
            {
                valueOfInputX = this.transform.position.x;
                deltaValueThird += valueOfInputX - startXPosition;
            }
            SwitchStatus();
        }
    }
    IEnumerator SwitchStatus()
    {
        yield return new WaitWhile(() => deltaValueFirst > 0f && deltaValueSecond > 0f && deltaValueThird > 0f);
        yield return new WaitForSeconds(5f);
        isReadyToStart = true;
    }
    void Update()
    {
        if (deltaValueFirst < 10f)
        {
            firstOK = true;
        }
        if (deltaValueSecond < 10f)
        {
            secondOK = true;
        }
        if (deltaValueThird < 10f)
        {
            thirdOK = true;
        }
        if (isReadyToStart)
        {
          if(firstOK && secondOK && thirdOK)
            {
                isOK = true;
            }
            else
            {
                isOK = false;
            }

        }

    }
}
