﻿using System.Collections;
using UnityEngine;
using Valve.VR;
using System;
public class SwitchingMechanics : MonoBehaviour
{
    float startXPosition;
    float lastXposition;
    public float deltaOfValue;
    public bool isDone;
    public float convertToNumbers;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "HandSphere")
        {
            if (SteamVR_Actions.default_InteractUI.GetStateDown(SteamVR_Input_Sources.Any))
            {
                startXPosition = other.transform.position.x;
                deltaOfValue += lastXposition != 0 ? startXPosition - lastXposition : 0;
                lastXposition = other.transform.position.x;
                convertToNumbers = Mathf.Round((deltaOfValue - 1) / (10 - 1));
            }
        }
    }
    private void Awake()
    {
        StartCoroutine(timeToChange());
    }
    IEnumerator timeToChange()
    {
        yield return new WaitWhile(() => deltaOfValue <= 0);
        while (true)
        {
            yield return new WaitForSeconds(5);
            isDone = deltaOfValue < 10 ? true : false;
        }
    }
}
