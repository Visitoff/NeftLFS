﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class ScenarioGas : MonoBehaviour
{
    [SerializeField]
    GameObject cabTeleportPoint;
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject camRig;
    GameObject spawnPoint;
    [SerializeField]
    HandClickGas handClickGas;
    List<SwitchingMechanics> rotationButtons;
    SwitchingMechanics swMech;
    Logs logs;
    Material success;
    Material lose;


    private void Awake()
    {
        logs = new Logs();
        GameObject handClickObj = GameObject.FindGameObjectWithTag("Sphere");
        handClickGas = handClickObj.GetComponent<HandClickGas>();
        player.transform.position = spawnPoint.transform.position;
        camRig.transform.position = spawnPoint.transform.position;

    }
    void Start()
    {
        StartCoroutine(scenario());
        StartCoroutine(scenarioBad());
    }
    IEnumerator scenarioBad()
    {
        yield return new WaitWhile(() => rotationButtons.All(x => x.isDone)&& swMech.deltaOfValue <= 0);
        logs.AddLog("Провалил обучение");
        //пизда рулю

    }
    void Update()
    {

    }
    IEnumerator scenario()
    {
        yield return new WaitWhile(() => handClickGas.triggerCommand);
        player.transform.position = cabTeleportPoint.transform.position;
        logs.AddLog("Переместился в комнату управления");
        yield return new WaitWhile(() => !rotationButtons.All(x => x.isDone));
        player.transform.position = spawnPoint.transform.position;
        logs.AddLog("Успешно закончил обучение");
    }

}
[Serializable]
public class Logs
{
    public List<string> logs;
    public Logs()
    {
        logs = new List<string>();
    }
    public void AddLog(string log)
    {
        logs.Add(log);

    }
}
